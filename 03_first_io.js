// first i/o

'use strict';

// ::: reference
// read a file:
// var buf = fs.readFileSync('/path/to/file');

// to string:
// var str = buf.toString()
// reference ends :::

function getNewLines(arg) {

    // arg should be a valid path to a file

    // read the file from arg
    // convert to a string
    // split the lines by a new line
    // count the number of lines
    var fs = require('fs'),
        fin = fs.readFileSync(arg),
        finString = fin.toString(),
        finLines = finString.split('\n'),
        lines = finLines.length - 1;

    return lines;
}

function main() {

    if (process.argv[2]) {

        console.log(getNewLines(process.argv[2]));

    } else {

        console.log('err\nusage: node 03_first_io,js <path to file>');
    }
}

main();
