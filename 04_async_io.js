// first i/o -- callbacks

'use strict';

// arg should be a valid path to a file
// fin = file input

var fs = require('fs'),
    path = process.argv[2],
    lines = 0;

function countLines(callback) {

    fs.readFile(path, function doneReading(err, fin) {

            var finString = fin.toString(),
                finLines = finString.split('\n');

            lines = finLines.length - 1;

            callback();
        });
}


function logNumberOfLines() {

    console.log(lines);

}

function main() {

    if (path) {

        countLines(logNumberOfLines);

    } else {

        console.log('err\nusage: node 04_async_io,js <path to file>');
    }
}

main();
