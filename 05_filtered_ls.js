// filtered ls
// filters the list of contents of a directory
// by the type specified

'use strict';

var fs = require('fs'),
    path = require('path'),
    pathToDir = process.argv[2],
    filterBy = process.argv[3],
    list = undefined;

function getDirListing(callback) {

    fs.readdir(pathToDir, function getList(err, dirContents) {

        list = dirContents;
        callback();

    });
}


function logDirContents() {

    var index;

    // remove the dot, if any
    if (filterBy[0] === '.') {

        filterBy = filterBy.replace('.', '');
    }

    // log by the filter specified
    for (index = 0; index < list.length; index += 1) {

        if (path.extname(list[index]).replace('.', '') === filterBy) {

            console.log(list[index]);
        }
    }
}

function main() {

    if (pathToDir && filterBy) {

        getDirListing(logDirContents);

    } else {

        console.log(
            'err: 2 args required\n' +
            'usage: node 04_async_io,js <path to file> <ext to filter>\n' +
            'example: node 04_async_io,js ../ txt'
        );
    }
}

main();
