// callback example

'use strict';

var fs = require('fs'),
    myNumber = undefined;

// will add 1 to the number in the text file
function addOne(callback) {
    
    fs.readFile('number.txt', function doneReading(err, fileContents) {
    
        myNumber = parseInt(fileContents);
        myNumber += 1;
        callback();
        });
}

function logMyNumber() {
    
    console.log(myNumber)

}

addOne(logMyNumber);
