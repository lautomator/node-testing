// filtered list module
// filters the list of contents of a directory
// by the type specified

// this module exports the results of the function getFilteredList()
// see the last line of code below

'use strict';

// module.exports = function () {

var fs = require('fs'),
    path = require('path'),
    pathToDir = process.argv[2],
    filterBy = process.argv[3],
    list = undefined;

function getDirListing(callback) {

    fs.readdir(pathToDir, function getList(err, dirContents) {

        list = dirContents;
        callback();

    });
}

function logDirContents() {

    var index,
        filteredList = [];

    // remove the dot, if any
    if (filterBy[0] === '.') {

        filterBy = filterBy.replace('.', '');
    }

    // log by the filter specified
    for (index in list) {

        if (list.hasOwnProperty(index)) {

            if (path.extname(list[index]).replace('.', '') === filterBy) {

                filteredList.push(list[index]);
            }
        }
    }

    console.log(filteredList);
}

function init() {

    if (pathToDir && filterBy) {

        getDirListing(logDirContents);

    } else {

        console.log('err: 2 args required\n' +
                'usage: node 04_async_io,js <path to file> <ext to filter>\n' +
                'example: node 04_async_io,js ../ txt');
    }
}

init();
